import { Controller } from "stimulus"
import { Factor, Decomposition } from "../decomposition"


class IterWrapper {
    iter: Iterator<Factor>;
    value?: Factor;

    move() {
        if (this.value) this.get_next_value();
    }

    get_next_value() {
        let val = this.iter.next();
        if (val.done) {
            this.value = undefined;
        } else {
            this.value = val.value;
        }
    }

    constructor(it: Iterator<Factor>) {
        this.iter = it;
        this.get_next_value();
    }
}

class MathRow {
    row: HTMLTableRowElement;
    empty: boolean;
    
    constructor(row: HTMLTableRowElement) {
        this.row = row;
        this.empty = true;
    }

    appendCell(text: string, cls: string = undefined) {
        let cell = document.createElement("td") as HTMLTableDataCellElement; 
        cell.innerHTML = text;
        if (cls) {
            cell.classList.add(cls);
        }
        this.row.appendChild(cell);
    }

    appendFactor(factor: number) {
        if (this.empty) {
            this.appendCell("");
        } else {
            this.appendCell("&middot;", "mult");
        }
        this.appendCell(factor.toString(), "factor");
        this.empty = false;
    }

    get id() {
        return this.row.getAttribute("id");
    }
}

class DecompositionRow extends MathRow {
    input_cell: HTMLTableDataCellElement;

    constructor(row: HTMLTableRowElement) {
        super(row);
        this.input_cell = this.row.getElementsByTagName("td")[0];
    }

    reset() {
        while (this.row.lastChild && this.row.lastChild != this.input_cell) {
            this.row.removeChild(this.row.lastChild);
        }
        this.empty = true;
    }
}

class ResultRow extends MathRow {

    reset() {
        while (this.row.firstChild) {
            this.row.removeChild(this.row.firstChild);
        }
        let cell = document.createElement("td") as HTMLTableDataCellElement; 
        cell.innerText = "";
        this.row.appendChild(cell);
        this.empty = true;
    }
}


export default class extends Controller {
    decompositions: Map<String, Decomposition>;


    *joined_factors() {
        var max_factor = 0;
        let iters = new Map<String, IterWrapper>();

        for (let d of this.decompositions) {
            max_factor = Math.max(max_factor, d[1].max_factor);
            let it = d[1].factors[Symbol.iterator]();
            iters.set(d[0], new IterWrapper(it));
        }

        let calc_min_factor = function() {
            let min_factor = 7919;

            for (let it of iters.values()) {
                if (it.value && it.value.factor < min_factor) {
                    min_factor = it.value.factor;
                }
            }

            return min_factor;
        }

        let min_factor = calc_min_factor();

        while(min_factor <= max_factor) {
            let result = new Map<String, Factor>();

            //  Get all factors having the current min_factor
            for (let it of iters) {
                let key = it[0];
                let wrapper = it[1];

                if (wrapper.value && wrapper.value.factor == min_factor) {
                    result.set(key, wrapper.value);
                    wrapper.move();
                }
            }

            yield result;

            min_factor = calc_min_factor();
        }
    }

    setup_input_handler(input: HTMLInputElement) {

        let row = input.closest("tr") as HTMLTableRowElement;
        let row_id = row.getAttribute("id");

        let source = input;
        let self = this;

        input.oninput = function(_) {
          self.decompositions.set(row_id,  new Decomposition(parseInt(source.value)));
          self.refresh_rows();
        }
    }

    refresh_rows() {

        let row_a = new DecompositionRow(this.element.querySelector("#row_a"));
        let row_b = new DecompositionRow(this.element.querySelector("#row_b"));
        let ggT = new ResultRow(this.element.querySelector("#ggT"));
        let kgV = new ResultRow(this.element.querySelector("#kgV"));

        row_a.reset();
        row_b.reset();
        ggT.reset();
        kgV.reset();

        let row_ids = [row_a.id, row_b.id];


        for (let factors of this.joined_factors()) {

            let all_factors = [];
            for (let key of row_ids) {
                let f = factors.get(key);
                if (f) {
                    all_factors.push(f.count);
                } else {
                    all_factors.push(0);
                }
            }

            let min_count = Math.min(...all_factors);
            let max_count = Math.max(...all_factors);

            for (let i=0; i < min_count; i++) {
                let factor = factors.values().next().value.factor.toString();
                ggT.appendFactor(factor);
            }
            for (let i=0; i < (max_count - min_count)*2; i++) {
                ggT.appendCell("");
            }
            
            for (let i=0; i < max_count; i++) {
                let factor = factors.values().next().value.factor.toString();
                kgV.appendFactor(factor);
            }

            this.udpate_decomposition_row(row_a, factors, max_count);
            this.udpate_decomposition_row(row_b, factors, max_count);
        }
    }

    udpate_decomposition_row(row: DecompositionRow, factors: Map<String, Factor>, max_count: number) {
        let f = factors.get(row.id);
        if (f) {
            for (let i=0; i < f.count; i++) {
                row.appendFactor(f.factor);
            }
            for (let i=0; i < max_count - f.count; i++) {
                row.appendCell("");
                row.appendCell("");
            }
        } else {
            for (let i=0; i < max_count; i++) {
                row.appendCell("");
                row.appendCell("");
            }
        }
    }

    connect() {
        this.decompositions = new Map();

        let inputs = this.element.getElementsByTagName("input");
        for (let input of inputs) {
            input.value = "";
            this.setup_input_handler(input);
        }
     }
}
